//
//  UserService.swift
//  MovieApp
//
//  Created by Internship on 12/14/21.
//

import Foundation
import UIKit


class UserService{
    
    let defaults = UserDefaults.standard
    
    func login(username:String,password:String)->Result<User,Error>{
        
        if username.count == 0 || password.count == 0{
            return .failure(Validation.emptyField)
        }
        
        if let user = fetchUserFromServerSide(username){
            if user.password == password{
                saveCurrentUser(user)
                return .success(user)
            }else{
                return .failure(Validation.usernameOrPasswordFails)
            }
        }else{
            return .failure(Validation.userNotFound)
        }
    }
    
    func registerUser(username:String,password:String,repeatPassword:String)->Result<User,Error>{
        
        if username.count == 0 || password.count == 0 || repeatPassword.count == 0{
            return .failure(Validation.emptyField)
        }
        
        if password != repeatPassword{
            return .failure(Validation.passworsDontMatch)
        }
        
        if let _ = fetchUserFromServerSide(username){
            return .failure(Validation.userAlreadyExist)
        }
        
        let user = User(username: username, password: password)
        createUserOnServerSide(user, username: username)
        saveCurrentUser(user)
        
        return .success(user)
    }
    
    @discardableResult
    func createUserOnServerSide(_ user: User, username: String) -> Bool {
        if let data = try? JSONEncoder().encode(user) {
            UserDefaults.standard.setValue(data, forKey: "__user_data_\(username)__")
            return true
        }
        return false
    }
    
    func fetchUserFromServerSide(_ username: String) -> User? {
        if let data = UserDefaults.standard.data(forKey: "__user_data_\(username)__") {
            if let user: User = try? JSONDecoder().decode(User.self, from: data) {
                return user
            }
        }
        return nil
    }
    
    @discardableResult
    func deleteUserOnServerSide(_ username: String) -> Bool {
        if let _ = UserDefaults.standard.data(forKey: "__user_data_\(username)__") {
            UserDefaults.standard.removeObject(forKey:  "__user_data_\(username)__")
            return true
        }
        return false
    }
    
    @discardableResult
    func updateUserWatchListOnServerSide(_ username: String, wathchList: [Int]) -> Bool {
        if let data = UserDefaults.standard.data(forKey: "__user_data_\(username)__") {
            if let user: User = try? JSONDecoder().decode(User.self, from: data) {
                user.watchList = wathchList
                
                if let data = try? JSONEncoder().encode(user) {
                    UserDefaults.standard.setValue(data, forKey: "__user_data_\(username)__")
                    return true
                }
            }
        }
        return false
    }
    
    func saveCurrentUser(_ user:User) {
        createUserOnServerSide(user, username: "current_user")
    }
    
    func loadCurrentUser() -> User? {
        return fetchUserFromServerSide("current_user")
    }
    
    func deleteCurrentUser(){
        if let user = loadCurrentUser(){
            deleteUserOnServerSide(user.username)
        }
        logoutCurrentUser()
    }
    
    func logoutCurrentUser(){
        deleteUserOnServerSide("current_user")
    }

}
