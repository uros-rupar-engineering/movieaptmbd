//
//  CastCell.swift
//  MovieApp
//
//  Created by Internship on 12/26/21.
//

import UIKit

class CastCell: UICollectionViewCell {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var mainvView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mainvView.layer.cornerRadius = 10
        mainvView.layer.borderWidth = 3
        mainvView.layer.masksToBounds = true
        mainvView.clipsToBounds = true
        mainvView.layer.borderColor = UIColor.white.cgColor
    }
}
