//
//  TrendingCell.swift
//  MovieApp
//
//  Created by Internship on 12/12/21.
//

import UIKit

class TrendingCell: UICollectionViewCell {
    
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
}
