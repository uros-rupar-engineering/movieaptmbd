//
//  GenreCellTableViewCell.swift
//  MovieApp
//
//  Created by Internship on 12/13/21.
//

import UIKit

class GenreCellTableViewCell: UITableViewCell {
    
    @IBOutlet weak var genreLabel: UILabel!
    
    @IBOutlet weak var holderView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        holderView.layer.borderWidth = 2
        holderView.layer.borderColor = UIColor.yellow.cgColor
        holderView.layer.cornerRadius = 10
        holderView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
