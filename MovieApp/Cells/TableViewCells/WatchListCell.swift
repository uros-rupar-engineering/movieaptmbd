//
//  WatchListCell.swift
//  MovieApp
//
//  Created by Internship on 12/22/21.
//

import UIKit

class WatchListCell: UITableViewCell {
    
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var mainTitle: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var markLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
      
        mainView.layer.cornerRadius = 10
        mainView.layer.masksToBounds = true
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
