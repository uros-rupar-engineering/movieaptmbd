//
//  WatchListVC.swift
//  MovieApp
//
//  Created by Internship on 12/22/21.
//

import UIKit
import Kingfisher
class WatchListVC: UIViewController {
    
    private(set) var userService: UserService!
    
    var movies:[MovieDetails] = []{
        didSet{
            
            DispatchQueue.main.async {
                self.movieTable.reloadData()
            }
            
        }
    }
    
    @IBOutlet weak var movieTable: UITableView!
    
    class var identifier: String { "WatchListVC" }
    
    class func instantiate(userService: UserService) -> WatchListVC {
        let storyboard = UIStoryboard(name: "Utils", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: identifier) as! WatchListVC
        
        vc.userService = userService
        return vc
    }
    
    class func instantiateWithNavigation(userService: UserService) -> UINavigationController {
        let vc = instantiate(userService: userService)
        
        let navigation = UINavigationController(rootViewController: vc)
        
        
        return navigation
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Watchlist"
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.movies = []
        
        for id in userService.loadCurrentUser()?.watchList ?? []{
            DataManager.fetchMovieDetails(url: Api.getMovieDetailsById(id: id), completion: { data in
                guard let movie = data else{ return }
                
                self.movies.append(movie)
                
            })
        }
    }
    
    @IBAction func gotToAccount(_ sender: Any) {
        let vc = AccountVC.instantiate(userService: userService)
        
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension WatchListVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "watchListCell", for: indexPath) as! WatchListCell
        
        let movie:MovieDetails = movies[indexPath.row]
        
        cell.mainTitle.text = movie.title
        cell.descriptionLabel.text = movie.overview
        cell.markLabel.text = "\(movie.voteAverage)"
        
        if let url = URL(string: DataManager.prepareImgUrl(path:movie.posterPath)){
            cell.mainImage.kf.setImage(with: url,placeholder: UIImage(systemName: "person"))
        }
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return tableView.frame.height * 0.3
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc  = MovieDetailsVC.instantiate(userService: self.userService, movieId: movies[indexPath.row].id)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
}
