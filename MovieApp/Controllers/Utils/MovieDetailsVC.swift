//
//  MovieDetailsVC.swift
//  MovieApp
//
//  Created by Internship on 12/12/21.
//

import UIKit
import Kingfisher
class MovieDetailsVC: UIViewController {
    
    
    private(set) var userService: UserService!
    private(set) var currentMovieId:Int!
    private(set) var movieDetails: MovieDetails!
    
    var actors:[Cast] = []{
        didSet{
            DispatchQueue.main.async {
                self.CastCollectionView.reloadData()
            }
        }
    }
    
    @IBOutlet weak var CastCollectionView: UICollectionView!
    @IBOutlet weak var mainPoster: UIImageView!
    
    @IBOutlet weak var mainTitle: UILabel!
    
    @IBOutlet weak var movieView: UIView!
    
    @IBOutlet weak var rateLabel: UILabel!
    
    @IBOutlet weak var addToWatchListButton: UIButton!
    
    @IBOutlet weak var descriptionLabel: UITextView!
    
    var isMovieAddedToWatchList = false
    
    class var identifier: String { "MovieDetailsVC" }
    
    class func instantiate(userService: UserService,movieId:Int) -> MovieDetailsVC {
        let storyboard = UIStoryboard(name: "Utils", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: identifier) as! MovieDetailsVC
        vc.userService = userService
        vc.currentMovieId = movieId
        return vc
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DataManager.fetchMovieDetails(url: Api.getMovieDetailsById(id: currentMovieId), completion: { data in
            guard let result = data else{ return }
            print(Api.getMovieDetailsById(id: self.currentMovieId))
            self.movieDetails = result
        })
        
        DataManager.fetchCastByMovieId(url: Api.getCastByMovie(movieID: currentMovieId), completion:{ data in
            guard let result = data else{ return }
            self.actors = result.cast
        })
    }

    @IBAction func addMovie(_ sender: UIButton) {
        
        if let user = self.userService.loadCurrentUser(){
            
            if let movie = movieDetails{
                if let index = user.watchList.firstIndex(of: movie.id){
                    user.watchList.remove(at: index)
                    sender.setImage(UIImage(systemName: "plus"), for: .normal)
                }else{
                    user.watchList.append(movie.id)
                    sender.setImage(UIImage(systemName: "checkmark"), for: .normal)
                }
                userService.saveCurrentUser(user)// u lokalu kesiranje
                userService.updateUserWatchListOnServerSide(user.username,wathchList: user.watchList)
            }
        }
    }
    
    func initializeUI(){
        title = movieDetails?.originalTitle
        if let url = URL(string: DataManager.prepareImgUrl(path: movieDetails?.posterPath ?? "")){
            
            mainPoster.kf
                .setImage(with: url)
            
        }
        mainTitle.text = movieDetails?.originalTitle ?? movieDetails?.title
        
        movieView.layer.cornerRadius = 9;
        addToWatchListButton.layer.cornerRadius =  addToWatchListButton.bounds.size.width/2
        addToWatchListButton.clipsToBounds = true
        
        mainPoster.layer.zPosition = -1
        rateLabel.text = "\(movieDetails?.voteAverage ?? 0)"
        descriptionLabel.text = "\(movieDetails?.overview ?? "")"
        
        let flag = self.userService.loadCurrentUser()?.watchList.contains(movieDetails?.id ?? -1) ?? false
        
        let image = UIImage(systemName: flag ? "checkmark" : "plus")
        addToWatchListButton.setImage(image, for: .normal)
    }
    
    override func viewDidLayoutSubviews() {
        initializeUI()
    }
}
extension MovieDetailsVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        actors.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "castCell", for: indexPath) as! CastCell
        
        let actor = actors[indexPath.row]
        
        cell.name.text = actor.name
        
        if let url = URL(string: DataManager.prepareImgUrl(path: actor.profilePath ?? "")){
            cell.profileImage.kf.setImage(with: url,placeholder: UIImage(systemName: "person"))
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = CastCollectionView.frame.width * 0.38
        let height = CastCollectionView.frame.height
        
        return CGSize(width: width, height: height)
    }
    
    
}
