//
//  ViewController.swift
//  MovieApp
//
//  Created by Internship on 12/10/21.
//

import UIKit
import Kingfisher

class MoviesVC: UIViewController {
    
    private(set) var userService: UserService!
    private(set) var currentGenre:Genre?
    
    class var identifier: String { "MoviesVC" }
    
    class func instantiate(userService: UserService) -> MoviesVC {
        let storyboard = UIStoryboard(name: "Utils", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: identifier) as! MoviesVC
        
        
        vc.userService = userService
        return vc
    }
    
    class func instantiate(userService: UserService,genre:Genre) -> MoviesVC {
        let storyboard = UIStoryboard(name: "Utils", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: identifier) as! MoviesVC
        
        vc.currentGenre = genre
        vc.userService = userService
        return vc
    }
    
    class func instantiateWithNavigation(userService: UserService) -> UINavigationController {
        let vc = instantiate(userService: userService)
        
        let navigation = UINavigationController(rootViewController: vc)
        
        
        return navigation
    }
    
    
    
    var trendingMovies:[Movie] = []{
        didSet{
            DispatchQueue.main.async {
                
                self.TrendingCollectionView.reloadData()
            }
        }
    }
    
    var moviesBayGenre:[MovieByGenre] = []{
        didSet{
            DispatchQueue.main.async {
                self.TrendingCollectionView.reloadData()
            }
        }
    }
    
    @IBAction func goToAccount(_ sender: Any) {
        let vc =  AccountVC.instantiate(userService: self.userService)
        navigationController?.pushViewController(vc, animated: true)
        //present(vc, animated: true, completion: nil)
    }
    
    
    @IBOutlet weak var TrendingCollectionView: UICollectionView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if let genre = currentGenre {
            DataManager.FetchMoviByGenres(url: Api.getMoviesByGenre(genreId: genre.id), completion: {data in
                guard let result = data else{ return}
                self.moviesBayGenre = result.results
                
            })
        }else{
            DataManager.fetchTrendingMovies(url: Api.API_TRENDING_MOVIES, completion: {data in
                guard let result = data else{ return }
                
                self.trendingMovies = result.results
                
            })
        }
        
        
        
        
    }
    
    override func viewDidLayoutSubviews() {
        
        if currentGenre == nil {
            title = "Trending"
        }else{
            title = currentGenre?.name
        }
    }
}
extension MoviesVC:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return currentGenre == nil ? trendingMovies.count : moviesBayGenre.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if currentGenre == nil{
            
            let movie = trendingMovies[indexPath.row]
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "trendingCell", for: indexPath) as! TrendingCell
            cell.rateLabel.text = "\( movie.voteAverage ) ⭐️"
            cell.titleLabel.text =  movie.name ?? movie.originalTitle
            
            if let url = URL(string: DataManager.prepareImgUrl(path: movie.posterPath)){
                cell.movieImage.kf.setImage(with: url)
            }
            
            cell.movieImage.layer.cornerRadius = 10
            cell.movieImage.layer.masksToBounds = true;
            return cell
            
        }else{
            let movie = moviesBayGenre[indexPath.row]

            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "trendingCell", for: indexPath) as! TrendingCell
            cell.rateLabel.text = "\( movie.voteAverage ) ⭐️"
            cell.titleLabel.text =  movie.title ?? movie.originalTitle
            if let url = URL(string: DataManager.prepareImgUrl(path: movie.posterPath)){
                cell.movieImage.kf.setImage(with: url)
            }

            cell.movieImage.layer.cornerRadius = 10
            cell.movieImage.layer.masksToBounds = true;
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = view.frame.width * 0.45
        let height = view.frame.height * 0.4
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc = currentGenre == nil ? MovieDetailsVC.instantiate(userService: self.userService, movieId: trendingMovies[indexPath.row].id) :  MovieDetailsVC.instantiate(userService: self.userService, movieId: moviesBayGenre[indexPath.row].id)
    
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
    
    
    
    
}


