//
//  GenreVC.swift
//  MovieApp
//
//  Created by Internship on 12/13/21.
//

import UIKit

class GenreVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    private(set) var userService: UserService!
    
    var genres:[Genre] = []{
        didSet{
            DispatchQueue.main.async {
                self.GenresTableView.reloadData()
            }
        }
        
    }
    @IBOutlet weak var GenresTableView: UITableView!

    class var identifier: String { "GenreVC" }
    
    class func instantiate(userService: UserService) -> GenreVC {
        let storyboard = UIStoryboard(name: "Utils", bundle: nil)
        
        let vc = storyboard.instantiateViewController(identifier: identifier) as! GenreVC
        vc.userService = userService
        return vc
    }
    
    class func instantiateWithNavigation(userService: UserService) -> UINavigationController {
        let vc = instantiate(userService: userService)
        return UINavigationController(rootViewController: vc)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Genres"
        
        DataManager.fetchGenres(url: Api.API_GENRES, completion:{ result in
            guard let result = result else{ return }
            self.genres = result.genres
        })
    }
    
    
    @IBAction func goToAccount(_ sender: Any) {
        let vc =  AccountVC.instantiate(userService: self.userService)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        self.genres.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "genreCell", for: indexPath) as! GenreCellTableViewCell
        
        cell.genreLabel.text = self.genres[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = MoviesVC.instantiate(userService: self.userService,genre: self.genres[indexPath.row])
        
        navigationController?.pushViewController(vc, animated: true)
        
    }
}




