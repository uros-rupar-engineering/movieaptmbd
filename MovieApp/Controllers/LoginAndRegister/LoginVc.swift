//
//  LoginVc.swift
//  MovieApp
//
//  Created by Internship on 12/14/21.
//

import UIKit

class LoginVc: UIViewController {
    
    var userService:UserService!
    
    @IBOutlet weak var usernameLabel: UITextField!
    @IBOutlet weak var passwordLabel: UITextField!
    @IBOutlet weak var login: UIButton!
    
    class var identifier: String { "LoginVC" }
    
    class func instantiate(userService:UserService) -> LoginVc {
        let storyboard = UIStoryboard(name: "LogInStoryboard", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: identifier) as! LoginVc
        vc.userService = userService
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func goToRegister(_ sender: Any) {
        let vc = RegisterVC.instantiate(userService: self.userService)
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func loginUser(_ sender: Any) {
        
        let result = userService.login(username: self.usernameLabel.text!, password: passwordLabel.text!)
        
        switch result {
        case .success(let user): presentMainUIFlow()
            
        case .failure(let error): handleError(error)
            
        }
    }
    
    override func viewDidLayoutSubviews() {
        initializeUI()
    }
    
    func  initializeUI(){
        usernameLabel.layer.borderWidth = 3
        usernameLabel.layer.cornerRadius = 10
        usernameLabel.layer.masksToBounds = true
        
        passwordLabel.layer.borderWidth = 3
        passwordLabel.layer.cornerRadius = 10
        passwordLabel.layer.masksToBounds = true
        
        login.layer.cornerRadius = 10
        login.layer.masksToBounds = true
    }
    
    func validateForm()->Bool{
        if let pass = self.passwordLabel.text,!pass.isEmpty,
           let usn = self.usernameLabel.text,!usn.isEmpty
        {
            return true
        }
        
        return false
    }
    func presentMainUIFlow(){
        let vc = MainTabBarController.instantiate(userService: self.userService)
        
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .crossDissolve
        present(vc, animated:false,completion: nil)
    }
    
    func handleError(_ error:Error){
        let alert = UIAlertController(title: "info", message:error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

}
