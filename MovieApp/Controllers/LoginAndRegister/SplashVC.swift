//
//  SplashVC.swift
//  MovieApp
//
//  Created by Internship on 12/21/21.
//

import UIKit

class SplashVC: UIViewController {
    
    var userService:UserService = UserService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let _ = userService.loadCurrentUser(){
            let vc = MainTabBarController.instantiate(userService: self.userService)
            vc.modalPresentationStyle = .fullScreen
            vc.modalTransitionStyle = .crossDissolve
            present(vc, animated:false,completion: nil)
            
        }else{
            let vc = LoginVc.instantiate(userService: self.userService)
            vc.modalPresentationStyle = .fullScreen
            vc.modalTransitionStyle = .crossDissolve
            present(vc, animated:false,completion: nil)
        }
        
        
    }
    
    
}
