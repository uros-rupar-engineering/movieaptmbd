//
//  RegisterVC.swift
//  MovieApp
//
//  Created by Internship on 12/15/21.
//

import UIKit

class RegisterVC: UIViewController {
    
    private(set) var userService: UserService!
    
    @IBOutlet weak var usernamelabel: UITextField!
    @IBOutlet weak var passwordLabel: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var repeatPasswodLabel: UITextField!
    
    
    class var identifier: String { "RegisterVC" }
    
    class func instantiate(userService: UserService) -> RegisterVC {
        let storyboard = UIStoryboard(name: "LogInStoryboard", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: identifier) as! RegisterVC
        vc.userService = userService
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func register(_ sender: Any) {
        
        let result = userService.registerUser(username: usernamelabel.text!, password: passwordLabel.text!, repeatPassword: repeatPasswodLabel.text!)
        switch result {
        
        case .success: presentMainUIFlow()
            
        case .failure(let error): handleError(error)
            
        }
    }
    
    @IBAction func backToLogin(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    override func viewDidLayoutSubviews() {
        initializeUI()
    }
    
    func  initializeUI(){
        
        usernamelabel.layer.borderWidth = 3
        usernamelabel.layer.cornerRadius = 10
        usernamelabel.layer.masksToBounds = true
        
        passwordLabel.layer.borderWidth = 3
        passwordLabel.layer.cornerRadius = 10
        passwordLabel.layer.masksToBounds = true
        
        repeatPasswodLabel.layer.borderWidth = 3
        repeatPasswodLabel.layer.cornerRadius = 10
        repeatPasswodLabel.layer.masksToBounds = true
        
        registerButton.layer.cornerRadius = 10
        registerButton.layer.masksToBounds = true
    }
    
    func presentMainUIFlow(){
        let vc = MainTabBarController.instantiate(userService: self.userService)
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .crossDissolve
        present(vc, animated:false,completion: nil)
    }
    
    func handleError(_ error:Error){
        print(error.localizedDescription)
        let alert = UIAlertController(title: "info", message:error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    
    
}
