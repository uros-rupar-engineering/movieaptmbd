//
//  MainTabBarController.swift
//  MovieApp
//
//  Created by Internship on 12/20/21.
//

import UIKit

class MainTabBarController: UITabBarController {
    
    private(set) var userService: UserService!
    
    
    @IBOutlet weak var mainBar: UITabBar!
    class var identifier: String { "MainTabBarController" }
    
    class func instantiate(userService: UserService) -> MainTabBarController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let vc = storyboard.instantiateViewController(identifier: identifier) as! MainTabBarController
        vc.userService = userService
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        ((self.viewControllers![0] as! UINavigationController).topViewController as! TrendingVC).userService = self.userService
        
        let trendingVC =  MoviesVC.instantiateWithNavigation(userService: userService)
        let tabBarItem = UITabBarItem(title: "Trending", image: UIImage(systemName: "house"), selectedImage:  UIImage(systemName: "house"))
        (trendingVC.topViewController as? MoviesVC)?.tabBarItem = tabBarItem
        self.viewControllers = [
            trendingVC,
            GenreVC.instantiateWithNavigation(userService: userService),
            WatchListVC.instantiateWithNavigation(userService: userService)
        ]
    }
}
