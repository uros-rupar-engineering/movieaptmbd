
import UIKit

class AccountVC: UIViewController {
    
    private(set) var userService: UserService!
    
    @IBOutlet weak var editUserCredentials: UIButton!
    @IBOutlet weak var deleteAccountButton: UIButton!
    class var identifier: String { "AccountVC" }
    
    @IBOutlet weak var firstNameLabel: UILabel!
    
    @IBOutlet weak var lastNameLabel: UILabel!
    
    @IBOutlet weak var dateOfBirthLabel: UILabel!
    
    class func instantiate(userService: UserService) -> AccountVC {
        let storyboard = UIStoryboard(name: "User", bundle: nil)
        
        let vc = storyboard.instantiateViewController(identifier: identifier) as! AccountVC
        vc.userService = userService
        return vc
    }
    
    @IBOutlet weak var usernameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        usernameLabel.text = userService.loadCurrentUser()?.username
        title = ""
        print(userService.loadCurrentUser()?.watchList)
    }
    
    
    @IBAction func logout(_ sender: Any) {
        warningAlert(warning: .logout)
    }
    
    
    @IBAction func deleeteUser(_ sender: Any) {
        warningAlert(warning: .delteUser)
    }
    
   
    
    func presentLoginVCIfNeeded(){
        
        if self.tabBarController?.isBeingPresented == true{
            self.dismiss(animated: true, completion: nil)
        }else{
            let vc = LoginVc.instantiate(userService: self.userService)
            vc.modalPresentationStyle = .fullScreen
            vc.modalTransitionStyle = .crossDissolve
            present(vc, animated: true, completion: nil)
        }
    }
    
    func warningAlert(warning:Warning){
        let alert = UIAlertController(title: "info", message:warning.alertTitle, preferredStyle: UIAlertController.Style.alert)
        
        switch warning {
        case .delteUser:
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.destructive, handler: {action in
                self.userService.deleteCurrentUser()
                self.userService.logoutCurrentUser()
                self.presentLoginVCIfNeeded()
            }))
        case .logout:
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.destructive, handler: {action in
                self.userService.logoutCurrentUser()
                self.presentLoginVCIfNeeded()
            }))
        }
        alert.addAction(UIAlertAction(title: "Cancle", style: UIAlertAction.Style.cancel, handler:nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewDidLayoutSubviews() {
        deleteAccountButton.layer.cornerRadius = 10
        deleteAccountButton.layer.masksToBounds = true
        editUserCredentials.layer.cornerRadius = 10
        editUserCredentials.layer.masksToBounds = true
        
        firstNameLabel.text = "Firstname"
        lastNameLabel.text = "Lastname"
        dateOfBirthLabel.text = "Date of birtsh"
    }
}
