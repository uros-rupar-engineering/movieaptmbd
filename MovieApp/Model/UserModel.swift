//
//  UserModel.swift
//  MovieApp
//
//  Created by Internship on 12/14/21.
//

import Foundation


class User:Codable{
    let username: String
    let password: String
    var watchList: [Int]
    
    init(username:String,password:String) {
        self.username = username
        self.password = password
        watchList = []
    }
}
