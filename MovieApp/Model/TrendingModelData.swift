//
//  TrendingModelData.swift
//  MovieApp
//
//  Created by Internship on 12/11/21.
//

import Foundation


struct TrendingMovieData: Codable {
    let page: Int
       let results: [Movie]
       let totalPages, totalResults: Int

       enum CodingKeys: String, CodingKey {
           case page, results
           case totalPages = "total_pages"
           case totalResults = "total_results"
       }
}

// MARK: - Result
struct Movie: Codable {
    let genreIDS: [Int]
        let id: Int
        let originalLanguage: OriginalLanguage
        let originalTitle: String?
        let posterPath: String
        let video: Bool?
        let voteAverage: Double
        let overview: String
        let releaseDate: String?
        let voteCount: Int
        let adult: Bool?
        let backdropPath: String
        let title: String?
        let popularity: Double?
        let mediaType: MediaType
        let firstAirDate, name, originalName: String?
        let originCountry: [String]?

        enum CodingKeys: String, CodingKey {
            case genreIDS = "genre_ids"
            case id
            case originalLanguage = "original_language"
            case originalTitle = "original_title"
            case posterPath = "poster_path"
            case video
            case voteAverage = "vote_average"
            case overview
            case releaseDate = "release_date"
            case voteCount = "vote_count"
            case adult
            case backdropPath = "backdrop_path"
            case title, popularity
            case mediaType = "media_type"
            case firstAirDate = "first_air_date"
            case name
            case originalName = "original_name"
            case originCountry = "origin_country"
        }
}

enum MediaType: String, Codable {
    case movie = "movie"
    case tv = "tv"
}

enum OriginalLanguage: String, Codable {
    case en = "en"
    case es = "es"
    case ja = "ja"
    case ko = "ko"
}
