//
//  GenreModel.swift
//  MovieApp
//
//  Created by Internship on 12/13/21.
//

import Foundation


struct GenreData: Codable {
    let genres: [Genre]
}

// MARK: - Genre
struct Genre: Codable {
    let id: Int
    let name: String
}
