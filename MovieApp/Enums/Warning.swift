//
//  Warning.swift
//  MovieApp
//
//  Created by Internship on 12/26/21.
//

import Foundation

enum Warning{
    case logout
    case delteUser
    
    var alertTitle:String{
        switch self {
        
        case .logout:
            return "Are You sure you want logout"
        case .delteUser:
            return "Are You sure you want to delete account"
        }
    }
}
