//
//  File.swift
//  MovieApp
//
//  Created by Internship on 12/22/21.
//

import Foundation

enum Validation:Error {
    
    case userAlreadyExist
    case passworsDontMatch
    case usernameOrPasswordFails
    case emptyField
    case userNotFound
    
    var alertTitle:String{
        switch self {
        
        case .userAlreadyExist:
            return "User with this Username already exist"
        case .passworsDontMatch:
            return "passwords dont match"
        case .usernameOrPasswordFails:
            return "Username or Password fail"
        case .emptyField:
            return "All fields are requred"
        case .userNotFound:
            return "User not found"
        }
    }
}

extension Validation:LocalizedError{
    var errorDescription: String?{
        
        switch self {
        case .userAlreadyExist:
            return  "User with this username already exist"
        case .passworsDontMatch:
            return "passwords dont match"
        case .usernameOrPasswordFails:
            return "Incorrect password"
        case .emptyField:
            
            return "All fields are requred"
        case .userNotFound:
            return "User not found"
        }
    }
    
}
