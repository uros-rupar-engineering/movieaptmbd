//
//  DataManager.swift
//  MovieApp
//
//  Created by Internship on 12/10/21.
//

import Foundation

struct DataManager {
    
    static func fetchTrendingMovies(url: String, completion: @escaping (TrendingMovieData?) -> Void){
        
        guard let apiURL = URL(string: url) else { return }
        let task = URLSession.shared.dataTask(with: apiURL){ (data, response, error) in
            
            if let error = error {
                print("Error accesing TMBD Api: \(error)")
            }
            
            guard let httpResponse = response as? HTTPURLResponse,
                  (200...299).contains(httpResponse.statusCode) else {
                print("Error with response, uninspected status code: \(response)")
    
                return
                
            }
            if let data = data, case let movies = try? JSONDecoder().decode(TrendingMovieData.self, from: data){
                completion(movies)
            }
        }
        task.resume()
    }
    
    static func fetchGenres(url: String, completion: @escaping (GenreData?) -> Void){
        
        guard let apiURL = URL(string: url) else { return }
        let task = URLSession.shared.dataTask(with: apiURL){ (data, response, error) in
            
            if let error = error {
                print("Error accesing TMBD Api: \(error)")
            }
            
            guard let httpResponse = response as? HTTPURLResponse,
                  (200...299).contains(httpResponse.statusCode) else {
                print("Error with response, uninspected status code: \(response)")
                
                return
                
            }
            if let data = data, case let genres = try? JSONDecoder().decode(GenreData.self, from: data){
                completion(genres)
            }
        }
        task.resume()
    }
    
    static func FetchMoviByGenres(url: String, completion: @escaping (MovieByGenreData?) -> Void){
        
        guard let apiURL = URL(string: url) else { return }
        let task = URLSession.shared.dataTask(with: apiURL){ (data, response, error) in
            
            if let error = error {
                print("Error accesing TMBD Api: \(error)")
                
            }
            
            guard let httpResponse = response as? HTTPURLResponse,
                  (200...299).contains(httpResponse.statusCode) else {
                print("Error with response, uninspected status code: \(response)")
                
                return
                
            }
            if let data = data, case let movies = try? JSONDecoder().decode(MovieByGenreData.self, from: data){
                completion(movies)
            }
        }
        task.resume()
    }
    static func fetchCastByMovieId(url: String, completion: @escaping (CastData?) -> Void){
        
        guard let apiURL = URL(string: url) else { return }
        let task = URLSession.shared.dataTask(with: apiURL){ (data, response, error) in
            
            if let error = error {
                print("Error accesing TMBD Api: \(error)")
                
            }
            
            guard let httpResponse = response as? HTTPURLResponse,
                  (200...299).contains(httpResponse.statusCode) else {
                print("Error with response, uninspected status code: \(response)")
                
                return
                
            }
            if let data = data, case let cast = try? JSONDecoder().decode(CastData.self, from: data){
                completion(cast)
            }
        }
        task.resume()
    }
    
    static func fetchMovieDetails(url: String, completion: @escaping (MovieDetails?) -> Void){
        
        guard let apiURL = URL(string: url) else { return }
        let task = URLSession.shared.dataTask(with: apiURL){ (data, response, error) in
            
            if let error = error {
                print("Error accesing TMBD Api: \(error)")
            }
            
            guard let httpResponse = response as? HTTPURLResponse,
                  (200...299).contains(httpResponse.statusCode) else {
                print("Error with response, uninspected status code: \(response)")
                
                return

            }
            if let data = data, case let details = try? JSONDecoder().decode(MovieDetails.self, from: data){
                completion(details)
            }
        }
        task.resume()
    }
    
    static func prepareImgUrl(path:String)->String{
        return "https://image.tmdb.org/t/p/w400/\(path)"
    }
}
