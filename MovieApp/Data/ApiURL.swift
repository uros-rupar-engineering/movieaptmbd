//
//  ApiURL.swift
//  MovieApp
//
//  Created by Internship on 12/13/21.
//

import Foundation

struct Api{
    
    static let API_KEY = "8054fbd1fe299bd85e3ea6400f6649d7"
    static let BASE_URL = "https://api.themoviedb.org/"
    
    static let API_TRENDING_MOVIES = "https://api.themoviedb.org/3/trending/all/week?api_key=\(API_KEY)"
    
    static let API_GENRES = "https://api.themoviedb.org/3/genre/movie/list?api_key=8054fbd1fe299bd85e3ea6400f6649d7&language=en-US"
    
    static func getMoviesByGenre(genreId:Int)->String{
        
        return "https://api.themoviedb.org/3/discover/movie?api_key=\(API_KEY)&with_genres=\(genreId)"
    }
    
    static func getCastByMovie(movieID:Int) -> String{
    
        let url = "https://api.themoviedb.org/3/movie/\(movieID)/credits?api_key=\(API_KEY)&language=en-US"
        return url
    }
    
    static func getMovieDetailsById(id:Int)->String{
        let url = "https://api.themoviedb.org/3/movie/\(id)?api_key=\(API_KEY)&language=en-US"
        
        return url
    }
    
}
